<?php
/**
 * Class CamelDelimiter
 */
class CamelDelimiter{
    /**
     * @param string $string
     * @return string
     */
    public function solution($string)
    {
        $needleString = '';
        //Here we check each symbol. It's useful when string is too large for system memory
        for ($i = 0; $i < strlen($string); $i++){

            if(ctype_upper ($string[$i])){
                $needleString .= ' ';
            }
            $needleString .= $string[$i];
        }
        return $needleString;
    }
}