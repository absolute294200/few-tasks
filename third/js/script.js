//main search function
function autocomplete(input, dictionary) {
    var autocomleteResults = [];
    var minimumCount = 1;
    var checkWordResult = [];
    var needleArray = [];
    input = getClearSymbols(input);
    console.log(input);

    for(var i = 0; i < dictionary.length; i++){

        checkWordResult = checkWord(input, dictionary[i], minimumCount);
        if(checkWordResult.length > 0){
            autocomleteResults.push(checkWordResult);
            minimumCount = checkWordResult[1];
        }
    }
    needleArray = clearInappropriateResults(autocomleteResults, minimumCount);

    return needleArray;
}

//check only one word for matches
function checkWord(input, word, minimumCount) {
    var symbolsFound = 0;
    for(var i = 0; i < word.length; i++){

        if(input[i].toLowerCase() == word[i].toLowerCase())
            symbolsFound++;
        else
            break;
    }
    //we will return the word and count of found symbols
    return symbolsFound >= minimumCount ? [word, symbolsFound] : [];
}

//clear results were have less matches than minimumCount
function clearInappropriateResults(autocomleteResults, minimumCount) {
    var needleArray = [];

    for(var i = 0; i < autocomleteResults.length; i++){

        if(autocomleteResults[i][1] == minimumCount)
            needleArray.push(autocomleteResults[i][0]);

        if(needleArray.length == 5)
            break;
    }
    return needleArray;
}

//replace non-alphabetic symbols
function getClearSymbols(input) {
    var pattern = new RegExp(/\W/g);
    return input.replace(pattern, '');
}

console.log(autocomplete('a$im', ['apple','ball','airplane','airport', 'AIRcraft', 'air', 'AirLines', 'airspace']));
