-- phpMyAdmin SQL Dump
-- version 4.0.10.12
-- http://www.phpmyadmin.net
--
-- Хост: webuni02.mysql.ukraine.com.ua
-- Время создания: Янв 24 2017 г., 02:46
-- Версия сервера: 5.6.27-75.0-log
-- Версия PHP: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `webuni02_few`
--

-- --------------------------------------------------------

--
-- Структура таблицы `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `department`
--

INSERT INTO `department` (`id`, `name`) VALUES
(1, 'Main office'),
(2, 'Sales department'),
(3, 'Mobile development'),
(4, 'Web development'),
(5, 'Design Studio');

-- --------------------------------------------------------

--
-- Структура таблицы `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` int(11) NOT NULL,
  `chief_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `salary` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `department_id` (`department_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `employee`
--

INSERT INTO `employee` (`id`, `department_id`, `chief_id`, `name`, `salary`) VALUES
(1, 1, 1, 'The boss', 100000),
(2, 1, 1, 'Grigory', 10000),
(3, 2, 2, 'Ivan', 9000),
(4, 2, 2, 'Greg', 8500),
(5, 2, 1, 'Hanna', 1000),
(6, 3, 2, 'John', 5000),
(7, 4, 2, 'Alex', 9900),
(8, 4, 3, 'Dmitry', 4500);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
