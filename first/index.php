<?php
include 'Classes/Spammer.php';

$spammer = new Spammer('hue');
echo 'OOP way:<br>';
echo $spammer->spam(6);

//Procedure(recursive) way
/**
 * @param $number
 * @return string
 */
function spam($number){
    $string = 'hue';
    return $number == 1 ? $string : $string . spam($number-1);
}

echo '<br><br>And here is the recursive way:<br>';
echo spam(6);