<?php

/**
 * Class Spammer
 */
class Spammer{

    private $string;

    /**
     * Spammer constructor.
     * @param $string
     */
    public function __construct($string)
    {
        $this->string = $string;
    }

    /**
     * @param $number
     * @return string
     */
    public function spam($number)
    {
        $needleString = '';

        for($i = 0; $i < $number; $i++){
            $needleString .= $this->string;
        }

        return  $needleString;
    }
}