<?php

/**
 * Class CarryCalculator
 */
class CarryCalculator{
    /**
     * @param array $input
     * @return array
     */
    public function carry($input)
    {
        $informationArray = array();
        foreach ($input as $pairString){
            $carryCounter =  $this->countCarryInPair($pairString);
            if($carryCounter == -1)
                $informationArray[] = "One of columns is too large";
            elseif ($carryCounter == 0)
                $informationArray[] = "No carry operations";
            elseif ($carryCounter > 0)
                $informationArray[] = $carryCounter . " carry operations";
        }
        return $informationArray;
    }

    /**
     * @param string $pairString
     * @return int
     */
    public function countCarryInPair($pairString)
    {
        $carryCounter = 0;
        $oldCarryDigit = 0;
        $numbers = $this->getNumberFromStringPair($pairString);
        if($this->isSmallEnoughPair($numbers[0], $numbers[1])){
            $numbers = $this->getMinimumAndMaximum($numbers);

            $lessNumber = $this->offsetLessNumber($numbers[0], $numbers[1]);
            $largerNumber = $numbers[1];

            for($i = strlen($largerNumber)-1; $i >= 0; $i--){
                $secondDigit = $lessNumber[$i] ? $lessNumber[$i] : 0;
                $oldCarryDigit = $this->getCarryNumberFromOneOperation($largerNumber[$i], $secondDigit, $oldCarryDigit);
                if($oldCarryDigit)
                    $carryCounter++;
            }
        }else{
            return -1;
        }
        return $carryCounter;
    }

    /**
     * @param string $lessNumber
     * @param string $largerNumber
     * @return string
     */
    public function offsetLessNumber($lessNumber, $largerNumber)
    {
        $difference = strlen($largerNumber) - strlen($lessNumber);
        if($difference > 0){
            for ($i = 0; $i < $difference; $i++){
                $lessNumber = '0' . $lessNumber;
            }
        }
        return $lessNumber;
    }

    /**
     * @param string $firstDigit
     * @param string $secondDigit
     * @param int $oldCarryDigit
     * @return int
     */
    public function getCarryNumberFromOneOperation($firstDigit, $secondDigit, $oldCarryDigit)
    {
        return (int)$firstDigit + (int)$secondDigit + $oldCarryDigit >= 10 ? 1 : 0;
    }

    /**
     * @param string $numbers
     * @return array
     */
    public function getMinimumAndMaximum($numbers) //it depends on string length of each number
    {
        if(strlen($numbers[0]) <= strlen($numbers[1]))
            return array($numbers[0], $numbers[1]);
        else
            return array($numbers[1], $numbers[0]);
    }

    /**
     * @param string $pairString
     * @return array
     */
    public function getNumberFromStringPair($pairString)
    {
        $firstNumberString = '';
        $secondNumberString = '';
        $secondNumberFlag = false;

        for ($i = 0; $i < strlen($pairString); $i++){

            if(is_numeric($pairString[$i])){
                if($secondNumberFlag)
                    $secondNumberString .= $pairString[$i];
                else
                    $firstNumberString .= $pairString[$i];
            }else{
                $secondNumberFlag = true;
            }
        }
        return array($firstNumberString, $secondNumberString);
    }

    /**
     * @param string $first
     * @param string $second
     * @return bool
     */
    public function isSmallEnoughPair($first, $second)
    {
        //int number size / 2
        return (int)$first < 32767/2 && (int)$second < 32767/2 ? true : false;
    }
}