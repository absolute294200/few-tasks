function zero(x) {
    return hasOperation(x) ? operation(0, x.operation, x.operand) : 0;
}
function one(x)  {
    return hasOperation(x) ? operation(1, x.operation, x.operand) : 1;
}
function two(x)  {
    return hasOperation(x) ? operation(2, x.operation, x.operand) : 2;
}
function three(x)  {
    return hasOperation(x) ? operation(3, x.operation, x.operand) : 3;
}
function four(x)  {
    return hasOperation(x) ? operation(4, x.operation, x.operand) : 4;
}
function five(x)  {
    return hasOperation(x) ? operation(5, x.operation, x.operand) : 5;
}
function six(x)  {
    return hasOperation(x) ? operation(6, x.operation, x.operand) : 6;
}
function seven(x)  {
    return hasOperation(x) ? operation(7, x.operation, x.operand) : 7;
}
function eight(x)  {
    return hasOperation(x) ? operation(8, x.operation, x.operand) : 8;
}
function nine(x)  {
    return hasOperation(x) ? operation(9, x.operation, x.operand) : 9;
}

function plus(x) {
    return {operation: '+', operand: x};
}
function minus(x) {
    return {operation: '-', operand: x};
}
function times(x) {
    return {operation: '*', operand: x};
}
function dividedBy(x) {
    return {operation: '/', operand: x};
}

function operation(firstOperand, mathOperation, secondOperand) {
    var result = 0;
    switch (mathOperation){
        case '+':
            result = firstOperand + secondOperand;
            break;
        case '-':
            result = firstOperand - secondOperand;
            break;
        case '*':
            result = firstOperand * secondOperand;
            break;
        case '/':
            result = firstOperand / secondOperand;
            break;
    }
    return result;
}

function hasOperation(x) {
    if(x){
        if(x.operand){
            return true;
        }
    }
    return false;
}

function checkCalculations() {
    console.log(three(times(five())));
    console.log(four(plus(eight())));
    console.log(eight(minus(two())));
    console.log(six(dividedBy(three())));
}

checkCalculations();
